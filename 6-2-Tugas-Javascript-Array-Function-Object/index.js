var title = "Tugas Pekan 6 Hari 2\nJavascript Array, Function, dan Object\n" 
console.log(title)


//Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()

daftarHewan.forEach(function(hewan){
    console.log(hewan)
 }) // <-- Jawaban Soal 1


console.log()
//OR

for (i=0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
} // <-- Jawaban Soal 1


console.log()
//Soal 2
function introduce(data) {
    return ("Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby)
} // <-- Jawaban Soal 2

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)


console.log()
//Soal 3
function hitung_huruf_vokal(text) {
    let matchingInstances = text.match(/[aeiou]/gi);
  
    if (matchingInstances) {
      return(matchingInstances.length)
  
    } else {
      return 0
    }
} // <-- Jawaban Soal 3

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")
  
console.log(hitung_1 , hitung_2) // 3 2


console.log()
//Soal 4
function hitung(num) {
    j=-2
    return j+2*num;    
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8