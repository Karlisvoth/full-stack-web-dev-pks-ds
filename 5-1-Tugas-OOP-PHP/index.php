<?php
    trait Hewan {
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi() {
          return "$this->nama sedang $this->keahlian <br><br>";
        }
      }
      
      trait Fight {
        public $attackPower;
        public $defencePower;
        public $result;
        // public $lawan;

        public function serang($lawan, $health, $attack, $defence) {
            function diserang($lawan, $health, $attack, $defence) {
                echo "$lawan sedang diserang <br>";
                $result=($health - $attack / $defence);
                echo "Sisa darah $lawan: ". $result."<br><br>";
          }
          echo "$this->nama sedang menyerang $lawan,<br>";
          diserang($lawan, $health, $attack, $defence);  
        }
      }
      
      class Elang {
        use Hewan, Fight;
         public function getInfoHewan() {
             echo "Jenis hewan: $this->nama<br>Darah: $this->darah<br>Jumlah kaki: $this->jumlahKaki<br>Keahlian: $this->keahlian<br>Attack power: $this->attackPower<br>Defence power: $this->defencePower<br><br>";
         }
      }
      
      class Harimau {
        use Hewan, Fight;
        public function getInfoHewan() {
            echo "Jenis hewan: $this->nama<br>Darah: $this->darah<br>Jumlah kaki: $this->jumlahKaki<br>Keahlian: $this->keahlian<br>Attack power: $this->attackPower<br>Defence power: $this->defencePower<br><br>";
        }
      }
      
      $elang = new Elang();

      $elang->nama = "elang";
      $elang->jumlahKaki = 2;
      $elang->keahlian = "terbang tinggi";
      $elang->attackPower = 10;
      $elang->defencePower = 5;

    //   echo "<br>";
      
      $harimau = new Harimau();

      $harimau->nama = "harimau";
      $harimau->jumlahKaki = 4;
      $harimau->keahlian = "lari cepat";
      $harimau->attackPower = 7;
      $harimau->defencePower = 8;

      $elang->getInfoHewan();
      echo $elang->atraksi();


      $harimau->getInfoHewan();
      echo $harimau->atraksi();
      
      $elang->serang($harimau->nama, $harimau->darah, $elang->attackPower, $harimau->defencePower) ;
      //$harimau->serang($elang->nama, $elang->darah, $harimau->attackPower, $elang->defencePower);
      
      ?>
