<?php

namespace App\Listeners;

use App\Mail\NewUser;
use App\Events\AccountCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpToEmail implements ShouldQueue
{   
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccountCreated  $event
     * @return void
     */
    public function handle(AccountCreated $event)
    {
        Mail::to($event->user->email)->send(new NewUser($event->user));
    }
}
