<?php

namespace App\Listeners;

use App\Mail\RegenerateOtp;
use App\Events\OtpRegenerate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResendOtpToEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpRegenerate  $event
     * @return void
     */
    public function handle(OtpRegenerate $event)
    {
        Mail::to($event->user->email)->send(new RegenerateOtp($event->user));
    }
}
