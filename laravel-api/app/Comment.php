<?php

namespace App;

use App\Traits\HasUuidID;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasUuidID;

    protected $table="comments";

    protected $fillable=["id", "content", "post_id", "user_id"];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function post(){
        return $this->belongsTo('App\Post');
    }
    
    public function user() {
        return $this->belongsTo('App\User');
    }

}
