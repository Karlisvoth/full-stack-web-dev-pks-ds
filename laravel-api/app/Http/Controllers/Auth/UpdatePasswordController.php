<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
                //response error validation
                if ($validator->fails()) {
                    return response()->json($validator->errors(), 400);
                }
        
                $user = User::where('email', $request->email)->first();
        
                $user->update([
                    'password' => Hash::make($request->password)
                ]);
        
                return response()->json([
                    'success'   => true,
                    'message'   => 'Password berhasil diubah',
                    'data'      => $user
                ]);
        
    }
}
