<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\OtpRegenerate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('test12');
        //set validation
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        //memanggil event OtpRegenerate
        event(new OtpRegenerate($user));

        if($user->otpCode) {
            $user->otpCode->delete();
        }

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while($check);

        $validUntil = Carbon::now()->addMinutes(5);
        $otp_code = OtpCode::create([
            'otp'           => $otp,
            'valid_until'   => $validUntil,
            'user_id'       => $user->id
        ]);

        return response()->json([
            'success'   => true,
            'message'   => 'OTP berhasil diregenerasi, silahkan periksa email anda',
            'data'      => [
                'user' => $user,
                'otp'   => $otp_code
            ]
        ]);
    }
}
