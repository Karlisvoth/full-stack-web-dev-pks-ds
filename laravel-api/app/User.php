<?php

namespace App;

use App\Traits\HasUuid;
// use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use HasUuid;
    use Notifiable;

// Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    protected $table="users";

    // protected $fillable=["id", "username", "email", "name", "password", "email_verified_at", "role_id"];
    protected $guarded=[];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function roles(){
        return $this->belongsTo('App\Role');
    }

    public function otpCode() {
        return $this->hasOne('App\OtpCode');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }

}
