<?php

namespace App;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    use HasUuid;

    // protected $table="otp_codes";

    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
