<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait HasUuidID
{
    protected static function bootHasUuidID()
    {
        // parent::boot();

        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->user_id = auth()->user()->id;
        });
    }
}
