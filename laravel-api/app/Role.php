<?php

namespace App;

use Illuminate\Support\Str;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasUuid;
    
    protected $table="roles";

    protected $fillable=["id", "name"];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';


    public function users(){
        return $this->hasMany('App\User');
    }
}
