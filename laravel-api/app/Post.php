<?php

namespace App;

use App\Traits\HasUuidID;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasUuidID;

    protected $table="posts";

    protected $fillable=["id", "title", "description", "user_id"];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
    
}
