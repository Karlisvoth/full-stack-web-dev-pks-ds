<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Saudara/i {{$comment->user->name}}, anda telah menulis komen di post "{{$comment->post->title}}" yang dibuat oleh {{$comment->post->user->name}} dengan isi sebagai berikut:</p>
    <p>{{$comment->content}}
</body>
</html>