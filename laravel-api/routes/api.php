<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function () {
    return auth()->user();
});

Route::group(['middleware' => ['auth:api']], function () {
    Route::apiResource('/post', 'PostController')->except([
        'index', 'show'
    ]);
    Route::apiResource('/comment', 'CommentController')->except([
        'index', 'show'
    ]);
});
/**
 * route resource post
 */
Route::apiResource('/post', 'PostController')->only([
    'index', 'show'
]);
/**
 * route resource roles
 */
Route::apiResource('/roles', 'RoleController');

/**
 * route resource comment
 */
Route::apiResource('/comment', 'CommentController')->only([
    'index', 'show'
]);

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function(){
    Route::post('/register', 'RegisterController')->name('auth.register');
    Route::post('/regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate');
    Route::post('/verification', 'VerificationController')->name('auth.verification');    
    Route::post('/update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('/login', 'LoginController')->name('auth.login');
});
