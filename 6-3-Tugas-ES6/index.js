//Soal 1
//Jawaban soal 1
const luas = (length, width) => {console.log(length * width)};

const keliling = (length, width) => {console.log(length*2 + width*2)};

//OR
const ukur = (length, width) => {console.log(`Luas: ${length*width}\nKeliling: ${length*2 + width*2}`)}

let x = 4;
let y = 13;

luas(x, y) //52
keliling(x, y) //34
ukur(x, y)

console.log()


//Soal 2
//Jawaban soal 2
const newFunction = (firstName, lastName) => {return {firstName, lastName, fullName(){(console.log(`${firstName} ${lastName}`))}}}  

newFunction("William", "Imoh").fullName()

console.log()


//Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

//Jawaban soal 3
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)

console.log()


//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)

//Jawaban soal 4
let combined = [...west, ...east]

//Driver Code
console.log(combined)

console.log()


//Soal 5
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

//Jawaban soal 5
let after = `Lorem ${view} dolor sit amet, consectur adipiscing elit, ${planet}`

// console.log(before)

console.log(after)

